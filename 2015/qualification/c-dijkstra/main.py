import sys

input_ = sys.stdin
#input_ = open('test.txt')
#input_ = open('A-small-practice.in.txt')
#input_ = open('A-large-practice.in.txt')

def input():
    return input_.readline()

def read_int():
    return int(input())

def read_line():
    return input().rstrip('\n')

def read_ints():
    return map(int, read_line().split())

def read_words(sep=' '):
    return read_line().split(sep)

# solution-specific functions
quat = [
    ['1', 'i', 'j', 'k'],
    ['i', '-1', 'k', '-j'],
    ['j', '-k', '-1', 'i'],
    ['k', 'j', '-i', '-1']
    ]

def idx(v):
    res = 0 # 1
    if v == 'i':
        res = 1
    elif v == 'j':
        res = 2
    elif v == 'k':
        res = 3
    return res

def mult(x,y):
    sign = 0
    if x[0] == '-':
        sign += 1
        x = x[1]
    if y[0] == '-':
        sign += 1
        y = y[1]
    res = quat[idx(x)][idx(y)]
    if res[0] == '-':
        sign += 1
        res = res[1]
    return ('-' if sign % 2 else '') + res

T = read_int()
for t in xrange(1, T + 1):
    # solution-specific code
    L, X = read_ints()

    st = read_line()

    def solve(pos, pat):
        cur = st[pos % L]
        ln = len(pat)
        while pos <= L * X - ln:
            pos += 1
            if cur == pat[0]:
                if ln == 1:
                    if pos == L*X:
                        return True
                else:
                    return solve(pos, pat[1:])
            cur = mult(cur, st[pos % L])
        return False
            
    print "Case #%d: %s" % (t,"YES" if solve(0, "ijk") else "NO")
