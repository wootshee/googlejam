import sys

input_ = sys.stdin
#input_ = open('test.txt')
#input_ = open('A-small-practice.in.txt')
#input_ = open('A-large-practice.in.txt')

def input():
    return input_.readline()

def read_int():
    return int(input())

def read_line():
    return input().rstrip('\n')

def read_ints():
    return map(int, read_line().split())

def read_words(sep=' '):
    return read_line().split(sep)

# solution-specific functions

T = read_int()
for t in xrange(1, T + 1):
    # solution-specific code
    D = read_int()
    P = read_ints()
    T = reduce(
        lambda m, l: min(m, reduce(lambda t, p: t + p / l - 1 + (1 if p % l else 0), filter(lambda x: x > l, P), l)),
        xrange(1, max(P) + 1), 10000)
    print "Case #%d: %d" % (t,T)
