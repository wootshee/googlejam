import sys

input_ = sys.stdin
#input_ = open('test.txt')
#input_ = open('A-small-practice.in.txt')
#input_ = open('A-large-practice.in.txt')

def input():
    return input_.readline()

def read_int():
    return int(input())

def read_ints():
    return map(int, input().split())

def read_words(sep=' '):
    return input().split(sep)

def read_line():
    return input().rstrip('\n')

# solution-specific functions

T = read_int()
for t in xrange(1, T + 1):
    # solution-specific code

    print "Case #%d: " % (t,)
