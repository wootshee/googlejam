import sys

def input():
    return sys.stdin.readline()

def read_int():
    return int(input())

def read_int_list():
    return map(int, input().split())

def read_word_list():
    return input().split()

def read_line():
    return input().rstrip('\n')

# solution-specific functions

cases = read_int()
for case in xrange(1, cases + 1):
    # solution-specific code
    line = read_line()
    reversed = line.split()
    reversed.reverse()
    print "Case #%d: %s" % (case, " ".join(reversed))
