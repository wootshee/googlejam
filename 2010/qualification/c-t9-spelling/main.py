import sys

def input():
    return sys.stdin.readline()

range1 = 'abcdefghijklmno'
range2 = 'pqrs'
range3 = 'tuv'
range4 = 'wxyz'

ranges = [(range1, 3, 2), (range2, 4, 7), (range3, 3, 8), (range4, 4, 9)]

def get_range(r):
    return r[0]

def get_button_size(r):
    return r[1]

def get_start_button(r):
    return r[2]

def translate(char):
    if char == ' ':
        return '0'
    for range in ranges:
        alphabet = get_range(range)
        if char in alphabet:
            idx = alphabet.index(char)
            return str(get_start_button(range) + idx / get_button_size(range)) * (idx % get_button_size(range) + 1)

cases = int(input())

for case in range(1, cases + 1):
    line = input()
    
    def translate_char(accum, char):
        out = translate(char)
        if accum.endswith(out[0]):
            accum += ' '
        return accum + out
    print "Case #%d: %s" % (case, reduce(translate_char, line.rstrip('\n'), ''))
