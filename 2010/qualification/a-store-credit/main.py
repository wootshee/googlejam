import sys

def input():
    return sys.stdin.readline()

def get_price(item):
    return item[0]

def get_index(item):
    return item[1]

def pre_process(arr):
    a = map(lambda i: (arr[i], i + 1), range(0,len(arr)))
    return sorted(a, key = lambda x: get_price(x))

def index_of_price(items, price, start):
    end = len(items) - 1
    while end > start:
        cur = start + (end - start) / 2
        cur_price = get_price(items[cur])
        if cur_price > price:
            end = cur - 1
        elif cur_price < price:
            start = cur + 1
        else:
            return get_index(items[cur])
    cur = start + (end - start) / 2
    cur_price = get_price(items[cur])
    if cur_price == price:
        return get_index(items[cur])
    return -1

def solve(credit, items):
    for i in range(0,len(items) - 1):
        price = credit - get_price(items[i])
        idx = index_of_price(items, price, i + 1)
        if idx != -1:
            return (get_index(items[i]), idx)
        
cases = int(input())

for case in range(1, cases + 1):
    credit = int(input())
    num_items = int(input())
    items = map(int, input().split())
    assert(len(items) == num_items)
    sol = solve(credit, pre_process(items))
    sol = map(int, sol)
    sol.sort()
    print("Case #%d: %d %d" % (case, sol[0], sol[1]))

    
