import sys

input_ = sys.stdin
#input_ = open('test.txt')
#input_ = open('A-small-practice.in.txt')
#input_ = open('A-large-practice.in.txt')

def input():
    return input_.readline()

def read_int():
    return int(input())

def read_int_list():
    return map(int, input().split())

def read_word_list():
    return input().split()

def read_line():
    return input().rstrip('\n')

# solution-specific functions

T = read_int()
for t in xrange(1, T + 1):
    # solution-specific code
    N = read_int()
    wires = [None] * N
    for n in xrange(0, N):
        wire = read_int_list()
        wires[n] = wire
    wires.sort(key = lambda x: x[0])
    def inv_count(a, s, e):
        if s >= e:
            return 0
        m = s + (e - s) / 2
        lc = inv_count(a, s, m)
        rc = inv_count(a, m + 1, e)
        c = 0
        # merge
        out = [None] * (e - s + 1)
        o = 0
        i = s
        j = m + 1
        while True:
            if i > m:
                out[o:len(out)] = a[j:e+1]
                break
            elif j > e:
                out[o:len(out)] = a[i:m+1]
                break
            if a[i] < a[j]:
                out[o] = a[i]
                i += 1
            else:
                out[o] = a[j]
                j += 1
                c += m - i + 1
            o += 1
        a[s:e+1] = out[0:len(out)]
        return c + lc + rc
            
    print "Case #%d: %d" % (t, inv_count(map(lambda x: x[1], wires), 0, len(wires)-1))
