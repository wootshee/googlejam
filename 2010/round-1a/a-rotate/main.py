import sys

input_ = sys.stdin
#input_ = open('test.txt')
#input_ = open('A-small-practice.in.txt')
#input_ = open('A-large-practice.in.txt')

def input():
    return input_.readline()

def read_int():
    return int(input())

def read_ints():
    return map(int, input().split())

def read_words():
    return input().split()

def read_line():
    return input().rstrip('\n')

# solution-specific functions
def pr(a):
    for i in xrange(len(a)):
        print "".join(a[i])

def rotate(a):
    c = ['.'] * len(a)
    r = [None] * len(a)
    for i in xrange(len(a)):
        r[i] = c[0:len(a)]
    for i in xrange(len(a)):
        jj = len(a) - 1
        for j in xrange(len(a)-1, -1, -1):
            if a[i][j] != '.':
                r[i][jj] = a[i][j]
                jj -= 1
    return r

def check(a, sc, sr, ic, ir, k):
    m = 1
    chr = a[sc][sr]
    i = sc + ic
    j = sr + ir
    while m < k:
        if chr != a[i][j]:
            break
        m +=1
        i += ic
        j += ir
    return m == k

def solve(a,k):
    wr = False
    wb = False
    for c in xrange(len(a)):
        for r in xrange(len(a)):
            chr = a[c][r]
            won = False
            if chr == '.' or chr == 'R' and wr or chr == 'B' and wb:
                continue
            if len(a) - c >= k and not won:
                #check vertical
                won = check(a, c, r, 1, 0, k)
            if  len(a) - r >= k and not won:
                #check horizontal
                won = check(a, c, r, 0, 1, k)
            if  len(a) - r >= k and len(a) - c >= k and not won:
                #check right diagonal
                won = check(a, c, r, 1, 1, k)
            if  r + 1 >= k and  len(a) - c >= k and not won:
                #check left diagonal
                won = check(a, c, r, 1, -1, k)
            if won and chr == 'R':
                wr = True
            elif won and chr == 'B':
                wb = True
            if wr and wb:
                break
        if wr and wb:
            break
    if wb and wr:
        return "Both"
    if wb:
        return "Blue"
    if wr:
        return "Red"
    return "Neither"

T = read_int()
for t in xrange(1, T + 1):
    # solution-specific code
    N,K = read_ints()
    board = [None] * N
    for n in xrange(N):
        line = read_line()
        board[n] = map(lambda x: x, line)
#    print "Case ", t, K
#    print "Board"
#    pr(board)
    rotated = rotate(board)
#    print "Rotated"
#    pr(rotated)
    print "Case #%d: %s" % (t, solve(rotated, K))
