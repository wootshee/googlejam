import sys

input_ = sys.stdin
#input_ = open('test.txt')
#input_ = open('A-small-practice.in.txt')
#input_ = open('A-large-practice.in.txt')

def input():
    return input_.readline()

def read_int():
    return int(input())

def read_int_list():
    return map(int, input().split())

def read_word_list():
    return input().split()

def read_line():
    return input().rstrip('\n')

# solution-specific functions
dirs = {}
def create_dir(dir):
    created = 0
    parts = dir.lstrip('/').split('/')
    parent = dirs
    for part in parts:
        if not parent.has_key(part):
            parent[part] = {}
            created += 1
        parent = parent[part]
    return created

T = read_int()
for t in xrange(1, T + 1):
    # solution-specific code
    N, M = read_int_list()
    for n in xrange(1, N + 1):
        create_dir(read_line())
    mkdirs = 0
    for m in xrange(1, M + 1):
        mkdirs += create_dir(read_line())
    dirs = {}
    print "Case #%d: %d" % (t,mkdirs)
