import sys

def input():
    return sys.stdin.readline()

def min_scalar_product(v1, v2):
    assert(len(v1) == len(v2))

    s1 = v1[:]
    s2 = v2[:]
    s1.sort()
    s2.sort()

    def min_term():
        if s1[0] < s2[0]:
            return s1.pop(0) * s2.pop()
        return s1.pop() * s2.pop(0)

    res = 0
    while len(s1):
        res += min_term()

    return res

cases = int(input())

for case in xrange(1, cases + 1):
    l = int(input())
    v1 = map(int, input().split())
    v2 = map(int, input().split())
    assert(len(v1) == l and len(v2) == l)
    print "Case #%d: %d" % (case, min_scalar_product(v1, v2))
