import sys, sets

input_ = sys.stdin
#input_ = open('A-small-practice.in.txt')
#input_ = open('A-large-practice.in.txt')

def input():
    return input_.readline()

def read_int():
    return int(input())

def read_int_list():
    return map(int, input().split())

def read_word_list():
    return input().split()

def read_line():
    return input().rstrip('\n')

# solution-specific functions

L, D, N = read_int_list()

dict = [""] * D

# build a dictionary
for d in xrange(0, D):
    word = read_line()
    dict[d] = word

mode = 'letter'

for case in xrange(1, N + 1):
    # solution-specific code
    word = read_line()
    indexes = range(0, D)
    new_indexes = []
    pos = 0
    for i in xrange(0, len(word)):
        if len(indexes) == 0:
            break
        char = word[i]
        
        if char == '(':
            mode = 'group'
        elif char == ')':
            mode = 'letter'
            pos += 1
            indexes = new_indexes
            new_indexes = []
        else:
            for j in xrange(0, len(indexes)):
                index = indexes[j]
                if dict[index][pos] == char:
                    new_indexes.append(index)
            if mode == 'letter':
                pos += 1
                indexes = new_indexes
                new_indexes = []

#    print word
    print "Case #%d: %d" % (case, len(indexes))
